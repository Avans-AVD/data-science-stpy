Allereerst de reden waarom ik vraag om feedback te geven is drieledig:

Het geeft de ontvanger van feedback waardevolle informatie over zijn eigen product, aanpak, probleemstelling.
Feedback mag zowel opbouwend zijn, maar ook bevestigend als je het juist goed vindt.
Feedback heeft een bondige argumentatie, zodat de ontvanger er iets  mee kan.
Feedback is een reactie en geen advies. Je geeft je visie.



========================= Feedback =========================

De Sting:

LA1:

- Tijdsduur                  : 14:45 - 15:15; 15:15 - 15:25 (Feedback)
- Duidelijk maken voorbeelden: JA

P1 - ?:
- Welkom woord
- Agenda met bullet points over inhoud presentatie
- Geeft achtergrond informatie over organisatie
- Geeft informatie over opdrachtgever, reden corona tijd en concurentie vormen halen uit webshop
- Stake holders in het bedrijf (intern, extern; primair en secundair)
P2 - Frank:
-
- ETL data is aangeleverd => konden hier ook verzoeken weg gelegd worden? -> Grootste gedeelte van de periode mee bezig geweest.
- Datamodel, relationeel over winkel, webhop, filialen en provincie
P3 - ?
- Presentatie demo - omzetcijfers vestigingen -> webshops
  - nergens terug te zien om welk tijdsbestrek het gaat
- toont netjes verschil tussen wel en niet corona tijd (winkel open / dicht)
    => Webshop omzet aanzienlijk hoger wanneer de winkel dicht is; is dit verschil terug te zien in de misgelopen omzet bij de winkels zelf?
    => Mogelijk gebruik van andere grafieken (lijn grafiek) zou de informatie nog beter inzichtelijk kunnen maken, het verloop van omzet cijfers staat
       nu gecombineert t.o.z. de andere webshops wat 'ruis' kan veroorzaken.
    => Tijd / eenheid / periode aanduidingen ook in grafieken tonen zouden de illustraties inzichtelijker maken; nu staat dit alleen in het filter.
    => Omzet per vestiging kaart met map dots erg leuk gedaan, geeft een goed beeld waar de meeste verkopen per vestigingslocatie gedaan worden.
        => Omzet per vestigingen kaart toont dot voor verkopen die gedaan zijn in de webshop, kan verwarrend zijn (mixen van verschillende gegevens)?
- Toelichting Sting eigen vervoer service goed toegelicht, heeft goede kennis van hoe de organisatie geregeld is.
- Groep gaat positief om met feedback en laat dit ook goed merken door complimenten te geven aan vraagsteller.
- Eigen risicos en aadachtspunten inzichtelijk gemaakt => datakwaliteit, informatiebehoeften; beheer & onderhoud => BI team - moet nieuwe data blijven krijgen, rechten => wie mag er bij?
- Aspecten -> Ethische aspecten; Security aspecten -> Gebruik locale database met wachtwoord, delen dmv one drive; overal waar de data op staat moet bitlocker op en mag niet gedeeld worden / usb.
    => hoe wil je dit afdwingen?

LA2:
- Tijdsduur                  : 15:25 - 15:30; 15:30 - 15:40 (Feedback)
- Duidelijk maken voorbeelden: NEE => Geen demo?

- Organisatie
- Probleemstelling -> voorspelling willen kunnen doen, op verwachte omzet in een willekeurige maand in de toekomst ivm stellen van budget.
- Stakeholders -> Nieuwe stakeholders voor LA2, Intern, extern; primair en secundair.
- Rolverdeling -> Veel gezamenlijk gerealiseerd, goed in groepsverband gedaan zodat iedereen er van kan leren.
- Voorspelmodel factoren -> Covid 19, Marketing, Seizoenen, Personeel (ziekte, of vertrek zonder opvul).
- Voorspelmodel data exploratie -> Missende waardes weg gehaald, op nul gezet of toegevoegd; o.b.v. eerdere maanden geextraploeerd, uitschieters aangepast. Bruikbaar maken van data.
- Model selectie -> dimensies verwijderd die niet gebruikt worden.
- Risicos en aandachtspunten -> Datakwaliteit en acceptatie (of er iets mee gedaan kan worden met het gerealiseerde product); onderhoud en beheer bij BI team; rechten zijn ook weer aandachtspunten.
- Aspecten -> Ethische aspecten; Security aspecten -> Gebruik locale database met wachtwoord, delen dmv one drive; overal waar de data op staat moet bitlocker op en mag niet gedeeld worden / usb.
    => hoe wil je dit afdwingen?

# gebruik vaktermen in presentatie, maar wel met goede toelichtingen.
# ontbreken van hoofd- en deelvragen?

Ignis:

# Erg lang, veel stof en soms iets te uitgebreid voor de gegeven tijdspan; had beter gepast als het beknopter verteld zou worden, ook te lang wezen hangen op eerste BI scherm.

LA1:

- Tijdsduur                  : 15:45 - 16:40 (Meerendeels vragen tijdens presentatie)
- Duidelijk maken voorbeelden: JA

P1:
- Geen welkom woord, direct wissel
- Probleem & vraagstuk
    => rommelig, toch eerst toelichting organisatie en stakeholders...?
    - probleemstelling: geen inzicht data / geen sturing mogelijk.
    - Hoofdvraag: Hoe kan BSA klanten voorzien van een kwalitatieve en betrouwbare BI-oplossing?
        - Deelvragen: welke gegevens, hoe wensen visualiseren, juiste plek data tonen en welke tools gebruiken?
- Uitleg stakeholders
- Oplossing -> Vertellen van data, data kwaliteit, transformatie, data inladen en BI-tool.
    => Vertelt hoe ze de data inzichtelijk hebben gemaakt door eerst data in te laten in de database en views te maken.
    - Hebben kolommen verwijderd die niet belangrijk waren, een centrale feiten tabel gemaakt (facturen).
    - Verhaal over de connector, vertelde dat ze momenteel nu een directe verbinding hebben met de database:
        => Wil dat zeggen dat ze de operationale database getransformeerd hebben met ETL? Een kopie? => betreft maatwerk, ieder eigen database.
- Operationaliseren
    - Opzet, moeilijke termen, 'on premise' => geen toelichting.
    - Voorbereiden database => moet bij elke klant, lijkt mij slecht voor de onderhoudbaarheid; wel onderbouwd met script maken voor onderhoudbaarheid.
- Demo
    => Geen idee verschil betekenis rapportage / dashboard?
    - Dashboard toont netjes rechts boven tijdspan; nette indeling pages; periode selector; plaats selector.
    => Gebruik van verschillende grafiek types op een manier waarop de data redelijk goed inzichtelijk gemaakt wordt, misschien nog verder uitdiepen met ander kleurschema.
    => Te plannen lijkt mooie voorspelling te geven in de data van toekomstig verloop in mogelijke omzet.
        => Factuur aantal werkt niet? Mislijdend? => Zijn nul facturen.
    => Factuur details op kunnen halen is erg tof uitgevoerd, geeft klant aanzienlijk meer informatie!
    - Vraag over privacy gevoelige data; geanonimseerd, maar omzet van het bedrijf niet; leek niet nodig te zijn en zitten afwijkingen in; wel besproken.

# last gehad van vast hangend beeld tijdens de presentatie.

LA2:

- Tijdsduur                  : 16:40 - 17:05
- Duidelijk maken voorbeelden: JA

- Probleem en vraagstuk: werving van personeel en capaciteit van depots in het land.
    - analiseren op basis van verandering personeel klantenbestand; te weinig personeel, klanten bij, tekort aan personeel.
    => Zeer uitgebreide informatie over probleem en vraagstukken; schept veel duidelijkheid over wat het doel achter LA2 is en wekt interesse naar de uitwerking.
- Data exploration -> Missende waardes verwijderd, outliers (setup moment van klant, veel gegevens in 1x invoeren).
    => Hoe hebben jullie de outliers in het project kunnen achterhalen : duidelijk zien van outliner door invoerdatum, kan gevalideerd worden door stakeholders.
- Model gekozen voor supervised learning, snelheid zou niet belangrijk zijn en i.c.m. data trainen en maandelijks evalueren.
- Operationaliseren van het voorspelmodel
    => goede lijst gemaakt van aandachtspunten en risicos; geeft een beeld dat er rekening gehouden wordt met het grotere geheel.



Kvk / Supermarkt:

- Tijdsduur                  : LA1: 17:15 - 17:40 + LA2: 17:42 - 18:03
- Duidelijk maken voorbeelden: JA

----
Databegrip:
- Productassortiment en transacties in CSV​
- SKU nummer is de primary key om inzichten te verkrijgen​
- Stock nummer validatie voor ontbrekende gegevens

Algoritme:
- Keuze apriori algoritme vanwege clustering​
- Unsupervised learning​
- Validate: toepassen van de business rules


Wat:
- keuze voor clustering ->
- probeert associatieregels te leren door patronen te vinden in set.
- gebruiken om toepassen van de business rules

Waarom Apriori:
- Onbeheerd (unsupervised learning)
- Geen specifieke kennis of input benodigd om het model te laten leren.
- Biedt parameters om uitkomsten mee te kunnen beperken / bepalen.
- Geschikt voor relationele databases zoals transactiedata.


Toelichting geven model a.h.v. presentatie en de functies.

Inzichten:
- Het aantal dat een combinatie terug moet komen, zal krachtiger zijn in het voorspellen van nieuwe combinaties. 
- De aanwezigheid van een combinatie ten opzichte van de transacties zal sterker zijn in het inzichtelijk maken van klanten koopgedrag.
    => Denk hierbij aan het analyseren van de effectiviteit van promoties. 

# ander leer traject genomen