# validation script to compare developed algorithm against existing library.
# https://docs.python.org/2/library/itertools.html#itertools.combinations
# https://en.wikipedia.org/wiki/Apriori_algorithm

# required libraries:
# pip install efficient-apriori

# required support of 50% of appearance over transaction count.
required_support = 0.50

# truth: [{eggs} -> {bacon}, {cheese} -> {bacon}]
transactions = [('eggs', 'bacon', 'cheese'),
                ('eggs', 'bacon', 'apple'),
                ('cheese', 'bacon', 'banana')]

# now add milk to the mix.
# truth: [{eggs} -> {bacon}, {cheese} -> {bacon}, {milk} -> {bacon}]
transactions = [('eggs', 'bacon', 'milk', 'cheese'),
                ('eggs', 'bacon', 'apple'),
                ('cheese', 'bacon', 'banana'),
                ('milk', 'bacon', 'chocopasta')]

from efficient_apriori import apriori as apriori_truth
truth, rules = apriori_truth(transactions, min_support=required_support, verbosity=1)

from formulas import apriori
dataset = apriori.occurrences(transactions, False)
combos = apriori.validate(dataset, min_length=1,  min_support=required_support)

print('\nPrinting memory contents (truth and own implementation):')
print('efficient-apriori:', truth)
print('implement-apriori:', combos)

print('\nExpected result equal to existing module:', apriori.verify(truth, combos))
assert apriori.verify(truth, combos), "Support and confidence levels differ, or an error was made in the apriori algorithm!"