import sys
import csv
from itertools import combinations
# https://docs.python.org/2/library/itertools.html#itertools.combinations
# https://en.wikipedia.org/wiki/Apriori_algorithm
# https://towardsdatascience.com/apriori-algorithm-for-association-rule-learning-how-to-find-clear-links-between-transactions-bf7ebc22cf0a
# https://www.datascience-pm.com/crisp-dm-2/
# assertions doen met de bestaande python library en tonen dat eigen toepassing werkt


# Aan de slag gaan en vervolgens er tegen aan lopen dat de verwachtingen en de benodigde eigenschappen om een
# verspellingsmodel op te bouwen is bij een casus waarbij geen informatiebron beschikbaar is, is het erg moeilijk
# om dit concreet sterk te maken i.c.m. generatie.

# Wie is de stake holder, wat is het business probleem en met welke interactie kan de markt hier veranderingen in brengen?
# Supermarkt manager wilt relatie tussen producten kunnen waarnemen (wat wordt samen verkocht) om daarop te kunnen anticiperen.
# Advies is om vervolg acties te realiseren op basis van uitkomsten mogelijke combinaties (hoe producten aanbieden, winstgevendere combi's).

# order_id;product_id;product_title;product_price;product_quantity;basket_order

def read_assortiment(filename):
    assortiment = {}
    csv_file = open(filename, 'r', encoding='utf-8')
    csv_reader = csv.reader(csv_file, delimiter=',')
    next(csv_reader, None)  # skip the headers
    for t_line in csv_reader:
        # TODO: add validation / migration of data to documentation.
        print(t_line)
        # initialise order:
        if t_line[0] not in assortiment:
            assortiment[t_line[0]] = {
                'title': t_line[1],
                'price': t_line[3],
            }
        # else:
        # append order product:
        # assortiment[t_line[0]][t_line[1]] = t_line[2]
    csv_file.close()
    return assortiment

def read_orders(filename):
    orders = {}
    csv_file = open(filename, 'r', encoding='utf-8')
    csv_reader = csv.reader(csv_file, delimiter=';')
    next(csv_reader, None)  # skip the headers
    for t_line in csv_reader:
        # TODO: add validation / migration of data to documentation.
        print(t_line)
        # initialise order:
        if t_line[0] not in orders:
            orders[t_line[0]] = {}
        # append order product:
        orders[t_line[0]][t_line[1]] = t_line[2]
    csv_file.close()
    return orders

def get_occurrences(itemsets):
    occurrences = {}
    for order, products in itemsets.items():
        for length in range(1, len(products)+1):
            # [",".join(map(str, combo)) for combo in combinations(itemset, length)]
            for combo in combinations(products, length):
                combo_map = ",".join(map(str, combo))
                if combo_map not in occurrences:
                    occurrences[combo_map] = 0
                occurrences[combo_map] += 1
    return occurrences

assortiment = read_assortiment('data/assortiment.csv')
orders = read_orders('data/transactions.csv')

from formulas import apriori
dataset = apriori.occurrences(orders)
# print(dataset)

# minimum combination length: 4 products, with a support of 4 within all transactions.
combos = apriori.validate(dataset, min_length=5,  min_support=4)

# minimum combination length: 3 products, with a support of a quantity of 5.
# combos = apriori.validate(dataset, 3,  5)

print('combinations (extra info):')
for level in combos:
    if len(combos[level]):
        for entry in combos[level]:
            print('--------------------------------------------------------------------')
            if type(entry) is tuple:
                print('strong link:', ' <-> '.join(entry), ', support:', combos[level][entry], ', f%: {:.3f}'.format(combos[level][entry] / dataset['transactions']))
                for sku in entry:
                    print('> sku:', sku, 'title:', assortiment[sku])
            else:
                print('strong item:', entry, ', support:', combos[level][entry], ', f%: {:.3f}'.format(combos[level][entry] / dataset['transactions']))
                print('> sku:', entry, 'title:', assortiment[entry])


# occurrences = get_occurrences(orders)

# min_length = 3 # todo:
# min_support = 4
# for combo in occurrences:
#     if occurrences[combo] >= min_support:
#         products = combo.split(',')
#         # skip combos that are too short (containing one item):
#         if len(products) < min_length:
#             continue
#         print(combo, '=>', occurrences[combo])
#         for product_sku in products:
#             print(assortiment[product_sku])

# opdracht datascience wordt: algoritme zelf ontwikkelen waar eigenlijk al libraries van zijn
# en valideren met een bestaande library om te controleren of daar de uitkomsten hetzelfde van zijn.

# hoe kunnen wij product clustering realiseren op basis van een eigen ontwikkeld algoritme?