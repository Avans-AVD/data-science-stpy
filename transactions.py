# -*- coding: utf-8 -*-
import csv
import random
import sys

# fix stout write buffer.
sys.stdin.reconfigure(encoding='utf-8')
sys.stdout.reconfigure(encoding='utf-8')

# parse product file and generate database structure.
product_database = []
with open('data/assortiment.csv', 'r', encoding='utf-8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for product in csv_reader:
        # parse fields
        product_sku = product[0]
        product_title = product[1]
        product_price = product[3]
        # print(product_sku, product_title, product_price)
        
        # assign values
        product_database.append({
            'sku': product_sku,
            'title': product_title,
            'price': product_price
        })

# quantity of products by possible pick chance (1's are more common).
pick_chances = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 4, 5]

csv_file = open('data/transactions.csv', 'w', newline='', encoding='utf-8')
csv_writer = csv.writer(csv_file, delimiter=';')
csv_writer.writerow(['order_id', 'product_id', 'product_title', 'product_price', 'product_quantity', 'basket_order'])

# generate shopping list transactions by random decisions.
print('Transactions:')
for t in range(1, 31):
    print('========== Shopping list (transaction {}) =========='.format(t))
    total_count = 0
    total_price = 0
    basket_products = []
    for basket in range(1, random.randint(1, 15)):
        # fetch a random product by index from 1 (skip header at 0).
        product_index = random.randint(1, len(product_database)-1)
        # skip duplicate entries:
        if product_index in basket_products:
            continue
        basket_products.append(product_index)

        product_count = pick_chances[random.randint(0, len(pick_chances)-1)]
        product_price = float(product_database[product_index]['price'])

        # write basket product line to screen.
        print('%d, %s, %d x %.2f => %.2f' % (
            basket,
            product_database[product_index]['title'],
            product_count,
            product_price,
            product_price * product_count
        ))

        # write record to csv export file.
        csv_writer.writerow([
            t,
            product_database[product_index]['sku'],
            product_database[product_index]['title'],
            product_price,
            product_count,
            basket
        ])

        # sum up total count of products and price.
        total_count += product_count
        total_price += product_price * product_count
    print('Basket total product count: %d, total cost: %.2f.' % (total_count, total_price))

csv_file.close()