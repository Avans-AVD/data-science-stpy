# 1. Aanleiding onderzoek 

De markt van supermarkten is een massamarkt waarbij schaalgrootte, efficiency en marketingkracht allesbepalende pijlers zijn voor succes en continuïteit.<br />
Een goede organisatie van de winkel, efficiënte operatie en het kennen van je klant zijn daarom erg belangrijk. Het traditionele verdienmodel van klassieke supermarkten staat wel onder druk.<br />
Ze krijgen steeds meer concurrentie van andere formats, waaronder zogenaamde “gemakswinkels”  en e-commerce. Supermarkten moeten zich meer gaan onderscheiden en dit vraagt creativiteit.<br />
<br />
(Detailhandel food: veranderende consument vraagt meer creativiteit, 2021)<br />
<br />
Bij retailers wordt omzet gegenereerd op basis van de product afzet. Datascience is in steeds meer beroepen beter toe te passen door de toenemende digitalisering.<br />
Door deze digitalisering kan productinformatie digitaal gemonitord worden, dit geldt ook voor kassa-systemen.<br />
Aankoop transacties kunnen worden opgeslagen, klantenkaarten worden gekoppeld om gebruikersprofielen op te bouwen en voorraadsystemen worden autonoom.<br />
Met alle vergaarde informatie is het onderzoeksdoel de omzet van de retailer te vergroten door meer potentiële afzet te genereren.<br />
De toepassing van digitalisering biedt kansen. Met behulp van datascience kan informatie verkregen worden van klanten en producten om zo gebruikersprofielen op te bouwen.<br />
Dit is van belang om voldoende afzet en daarmee ook omzet te blijven genereren en te overleven in de markt.<br />

# 2. Probleemanalyse 

In deze fictieve case weet supermarkt MYS dat “gemak” de trend is. Iedereen is druk en wil zijn boodschappen zo snel mogelijk,<br />
liefst zonder na te denken doen (Dé 3 foodtrends van 2021 (en de invloed van Corona), 2021).<br />
Van daaruit is de wens om inzicht te verkrijgen in welke producten klanten vaak samen kopen,<br />
zodat de supermarkt hierop in kan spelen en door bijvoorbeeld de inkoop, schapindeling of recepten en aanbiedingen aan te passen op deze inzichten.<br />

# 3. Centrale vraagstelling 

**Hoe kan de retailer relaties tussen producten voorspellen en hierop in kan spelen om de afzet te vergroten?**

1. Inzichtelijk maken welke producten gezamenlijk gekocht worden om combinaties te genereren.
2. Inzichtelijk maken welke producten aanvullend interessant zijn voor de klant (koopgedrag: slagroom op appeltaart op basis van transacties).
3. Optioneel: Inzichtelijk maken welke aanvullende producten gekocht worden bij een bestaand recept (koopgedrag: sweet chicken chili recept aanvullen met cashewnoten op basis van recepten).
4. Optioneel: Inzichtelijk maken welke alternatieve producten gecombineerd kunnen worden met recepten (alternatieve producten in het recept aan te bieden met meer marge: Lassie toverrijst in plaats van Jumbo rijst).

# 4. Omschrijving van het eindresultaat 

Om een voorspellingsmodel te creëren moet er een formule ontstaan die aansluit op de analyse van de probleemstelling.<br />
Om dit te kunnen realiseren is er data nodig en moet er een voorspellingsmodel gebouwd dat aankopen analyseert en product relaties moet gaan herkennen.<br />
Deze informatie kan vervolgens op verschillende manieren strategisch voordeel creëren.<br />

**De dataset bestaat uit:**
- Productaanbod supermarkt (CSV, scraped).
- Aankoop transactie geschiedenis op basis van aankopen en aantallen (gegenereerd).
- Algoritme dat transacties analyseert en statistieken vormt o.b.v. gecombineerde producten
- Optioneel: recepten

**De aspecten die hierbij van belang zijn, zijn:**
- Hoe kunnen gerelateerde producten herkend worden?
- Hoe reëel is het gedetecteerde verband (controle ten opzichte van het recept)?
- Optie aanvullend product in formule kunnen toevoegen (kochten ook)?
- Optioneel: Klopt de verhouding van het aantal van de gerelateerde producten?
