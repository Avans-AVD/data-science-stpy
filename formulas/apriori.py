from itertools import combinations
# https://docs.python.org/2/library/itertools.html#itertools.combinations
# https://en.wikipedia.org/wiki/Apriori_algorithm
# assertions doen met de bestaande python library en tonen dat eigen toepassing werkt

def occurrences(transactions, multi=True, max_depth=999):
    occurrences = {'transactions': len(transactions), 1:{}}
    for t_index in transactions.__iter__():
        if multi:
            products = transactions[t_index]
        else:
            products = t_index
        # for products in transactions[t_index].__iter__():
        # print(products)
        products = sorted(set(products))
        # print('sorted:', products)
        # iterate for each position in product list (1 by one, first position):
        for index in range(0, len(products)):
            # print(index, '=>', products[index])
            # assign the product to the occurrences list and increment count.
            if products[index] not in occurrences[1]:
                occurrences[1][ products[index] ] = 0
            occurrences[1][ products[index] ] += 1

        # for every possible length this product combination offers
        for index in range(2, len(products)+1):
            if index > max_depth:
                print('max depth has been reached, skipping deeper combinations.')
                break
            if index not in occurrences:
                occurrences[index] = {}
            # print('c', index, ':')
            # try to make every possible combination with the given length
            for combo in combinations(products, index):
                # print(index, '=>', combo)
                # sort products in combination alphabetically to make [1,2] and [2,1] match.
                if combo not in occurrences[index]:
                    occurrences[index][ combo ] = 0
                occurrences[index][ combo ] += 1
    return occurrences

# occurrences: dictionary structure containing level => links associations.
# min_length: min length of combinations that are considered as valid.
# min_support: can contain float or number.
# number specifies what quantity an occurrence should appear.
# float specifies what percentage a occurrence should appear against transaction count.
def validate(occurrences, min_length=2, min_support=0):
    print('Analysing %d transactions, against a validation model with a minimum of %d products and %.2f support...' % (
        occurrences['transactions'], min_length, min_support)
    )
    valid_results = {}
    for level in occurrences:
        # skip non-level data:
        if not isinstance(level, int):
            continue
        
        if level not in valid_results:
            valid_results[level] = {}
        for itemset in occurrences[level]:
            quantity = occurrences[level][itemset]
            # print('depth: %d, items: %s, quantity: %d' % (
            #     level,
            #     itemset,
            #     quantity
            # ))

            if level >= min_length:
                # handle min_support by actual occurrence quantity.
                if min_support >= 1 and quantity >= min_support:
                    valid_results[level][itemset] = quantity
                # handle min_support by floating percentage.
                if min_support >= 0 and min_support <= 1:
                    presence = quantity / occurrences['transactions']
                    if presence >= min_support:
                        valid_results[level][itemset] = quantity

    print('Combinations within definition(s):')
    for level in valid_results:
        if len(valid_results[level]):
            for entry in valid_results[level]:
                if type(entry) is tuple:
                    print('strong link:', ' <-> '.join(entry), ', support:', valid_results[level][entry])
                else:
                    print('strong item:', entry, ', support:', valid_results[level][entry])

    return valid_results

# verify apriori results against each other.
# notify differences and print out informative messages.
def verify(truth, combos):
    valid = True
    # iterate over every depth of combo length.
    for level in combos:
        if not len(combos[level]):
            # skip empty depths and rapport depth length differences.
            if level in truth and len(truth[level]):
                print('Results miss match on level: %d' % (level))
                valid = False
                continue
            continue
        if level not in truth:
            print('Truth does not hold entries at level %d' % (level))
            valid = False
            continue
        # iterate over every key within truth depth.
        for key in truth[level]:
            # convert level 1 tuple to string key to match combo structure.
            t_key = ''.join(key) if level == 1 else key
            t_val = truth[level][key]
            if t_key not in combos[level]:
                print(t_key, 'of truth not found in combos!')
                valid = False
                continue
            c_val = combos[level][t_key]

            # compare number of matches within algorithm result:
            if t_val != c_val:
                print(t_key, 'value is different:', t_val, c_val)
                valid = False
                continue
    return valid